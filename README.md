# Puppet Control Repository

Dépôt central de gestion de de puppet.

__IMPORTANT:__

*Après clonage du dépôt, merci d'executer le script `setup-environment.sh` qui installe les __hooks pre-commit__, le linter de style pour éviter de commiter des changements contenant des __erreurs de syntaxe__ ou autre:*

```[repo]/misc/setup-environment.sh```


- L'import des modules est géré via le fichier Puppetfile
- Les environnements Puppet sont générés automatiquement depuis les branches git créées sur ce dépôt.
- Chaque modification apportée sur ce dépôt déclenche des vérifications dans l'outil de CI (jenkins.dev.uqam.ca, projet infra-puppet-controlrepo-dev)
  (Le déclencheur jenkins est configuré dans bitbucket)

---

* Si vous désirez déclencher la construction sur Jenkins directement au moment ou vous poussez les modifications sur le dépôt, il est possible de créer un alias dans .git/config:

```
  [alias]
  xpush = !git push && java -jar jenkins-cli.jar -s "https://jenkins.dev.uqam.ca" -http -auth "jenkins_user:jenkins_api_key" -noKeyAuth build 'infra-puppet-controlrepo/puppet-controlrepo - pipeline/base' -c
  zpush = !git push && java -jar jenkins-cli.jar -s "https://jenkins.dev.uqam.ca" -http -auth "jenkins_user:jenkins_api_key" -noKeyAuth build 'infra-puppet-controlrepo/puppet-controlrepo - pipeline/base' -f -c
```
   *__Requis__: wget https://jenkins.dev.uqam.ca/jnlpJars/jenkins-cli.jar*
