## site.pp ##
#hiera_include('classes')

# https://forge.puppet.com/cristifalcas/etcd/readme
File { backup => false }

node default {
  include motd
}


node puppetmaster.test {
  include motd
}


# Labunix: test resource files
node 'C3000L.labunix.dev' {

#declarer profile::files_labunix (avec comme parametre: deb_labunix_C3000L) avant profile::packages_labunix, car certains packages sont installes a partir des .deb

include apt


  class { 'profile::files_labunix':
    files => lookup('files_labunix',Hash,'hash'),
  }

  class { 'profile::apt_sources_labunix':
    apt_repos => lookup('apt_repos_labunix_C3000L',Hash,'hash'),
  }

  class { 'profile::apt_ppas_labunix':
    apt_ppas => lookup('apt_ppas_labunix_C3000L',Array[String]),
  }

  class { 'profile::packages_labunix':
    packages => lookup('packages_labunix_C3000L',Hash,'hash'),
  }

  class { 'profile::users_labunix':
    users => lookup('users_labunix',Hash,'hash'),
  }
  
  class { 'profile::ssh_authorized_keys_labunix':
    authorized_keys => lookup('ssh_authorized_keys_labunix_salle_t',Hash,'hash'),
  }
  
  class { 'profile::crons_labunix':
    crons => lookup('crons_labunix_salle_t',Hash,'hash'),
  }
}

#Labunix:  pour le poste specifique C39368L.ens.uqam.ca
node 'C39368L.ens.uqam.ca' {
  class { 'profile::users_labunix':
    users => lookup('users_labunix_C39368L',Hash,'hash'),
  }
  class { 'profile::packages_labunix':
    packages => lookup('packages_labunix_C39368L',Hash,'hash'),
  }
}


#Labunix:  pour tout les clients  dont le nom a la forme C (ou c) suivi de 5 chiffres, suivi de L (ou l), suivi de .ens.uqam.ca
node /^(C|c)\d{5}(L|l)\.ens\.uqam\.ca$/ {
  $salle = $facts['salle_labunix']
  case $salle {
    'Salle_T': { 
                 class { 'profile::users_labunix':
                   users => lookup('users_labunix_salle_t',Hash,'hash'),
                 }
                 class { 'profile::packages_labunix':
                   packages => lookup('packages_labunix_salle_t',Hash,'hash'),
                 }
               }
    'Salle_E': {  
                 class { 'profile::users_labunix':
                   users => lookup('users_labunix_salle_e',Hash,'hash'),
                 }
                 class { 'profile::packages_labunix':
                   packages => lookup('packages_labunix_salle_e',Hash,'hash'),
                 }
               }
    default: { 
                 class { 'profile::users_labunix':
                   users => lookup('users_labunix',Hash,'hash'),
                 }
                 class { 'profile::packages_labunix':
                   packages => lookup('packages_labunix',Hash,'hash'),
                 }
             }
  }
}



# Maitres kubernetes (test et prod)
node /^kube-244-8(9|1)\.si\.uqam\.ca$/ {
  include profile::base
  include profile::firewall
  include role::kubernetes::etcd
  include role::kubernetes::etcd::bootstrap
  include role::kubernetes::master
  include role::kubernetes::autoload
  include role::kubernetes::kubeconfigs
}

# noeuds kubernetes + etcd (test et prod)
node /^kube-244-8(8|7|6|2|3|4)\.si\.uqam\.ca$/ {
  include profile::base
  include profile::firewall
  include role::kubernetes::etcd
  include role::kubernetes::etcd::join
  include role::kubernetes::worker
}

# noeud kubernetes !etcd (tous)
node /^kube-244-.*\.si\.uqam\.ca$/ {
  include profile::base
  include profile::firewall
  include role::kubernetes::etcd
  include role::kubernetes::worker
}


node 'etcd-244-101.si.uqam.ca' {
  include profile::base
  include profile::firewall
  include role::kubernetes::etcd
  include role::kubernetes::etcd::bootstrap
}

node /^etcd-244.*\.si\.uqam\.ca$/ {
  include profile::base
  include profile::firewall
  include profile::kubernetes
  include role::kubernetes::etcd
  include role::kubernetes::etcd::join
}

node 'memoire.si.uqam.ca' {
  include profile::base
  include profile::firewall
  include profile::networker
}

node 'moka.labunix.uqam.ca' {
  include profile::base
  include profile::firewall
  include profile::networker
}

node 'netconf.telecom.uqam.ca' {
  include profile::base
  include profile::networker
  # include profile::samba
  include role::netconf
}

node 'node0-esg.si.uqam.ca' {
  include profile::base
  include profile::networker
}

node 'proxy0-esg.si.uqam.ca' {
  include profile::base
  include profile::networker
}

node 'sap-esg.si.uqam.ca' {
  include profile::base
  include profile::networker
}

node 'test-infra-244-107.si.uqam.ca' {
  include profile::base
  include profile::firewall
  include profile::networker
}
