# Configuration du serveur netconf
class role::netconf {
    include profile::samba
    include profile::kmod
    include profile::tftp
    include profile::pureftp
  }
