# Role Etcd
class role::kubernetes::etcd {
  include profile::kubernetes
  include profile::etcd
}

# Role to boostrap etcd cluster
class role::kubernetes::etcd::bootstrap {
  include profile::kubernetes
  include profile::etcd
  include profile::etcd::bootstrap
}

# Role to join etcd cluster
class role::kubernetes::etcd::join {
  include profile::kubernetes
  include profile::etcd
  include profile::etcd::join
}
