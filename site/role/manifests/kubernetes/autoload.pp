# Class
class role::kubernetes::autoload {
  include profile::kubernetes
  include profile::kubernetes::autoload
  include profile::kubernetes::namespaces
  include profile::kubernetes::rolebindings
  include profile::kubernetes::volumes
}
