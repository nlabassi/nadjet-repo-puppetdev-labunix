Facter.add(:homelocal) do
  setcode do
    if File.directory? '/homelocal'
      'homelocal'
    else
      'home'
    end
  end
end
