Facter.add(:timestamp) do
  setcode do
    time = Time.new
    timestamp=time.strftime("%Y%m%d%H%M%S")
  end
end
