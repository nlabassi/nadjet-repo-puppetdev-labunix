Facter.add(:ipaddress_backup) do
  setcode "ip route | egrep '^192\.168\.[8,9,10,11,12,13,14,15,16,17,18,19]\.|^132\.208\.220\.' | awk '{print $9}'"
end
