Facter.add(:security_zone) do
  setcode do
    gateway = %x(ip route | awk '/default/{print $3}')
    case gateway
    when /132.208.132.1|132.208.211.240|132.208.147.1|132.208.219.240|132.208.118.1|132.208.250.240/
      'Public'
    when /132.208.246.1|132.208.244.1/
      'DMZ'
    when /172.20.224.1|172.20.224.240/
      'Server_Farm'
    else
      nil
    end
  end
end
