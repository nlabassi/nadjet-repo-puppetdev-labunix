# Manages /etc/hosts entries
class profile::hosts {
  class { 'hosts': }
}
