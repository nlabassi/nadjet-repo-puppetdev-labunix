#site/profile/manifests/files_labunix.pp
class profile::files_labunix ($files) {
  $files.each | String $file_name, Hash $attrs | 
  {
     file { $file_name:
       * => $attrs,
    }
  }

}
