# Class profile::ntp
class profile::ntp {

  $ntpserver = lookup({'name'=>'ntp::server','default_value'=> undef})

  class { 'ntp':
    servers => $ntpserver
  }
}

# Class profile::ntpserver
class profile::ntpserver {

  $ntp_atomic = lookup({'name'=>'ntp::server','default_value'=> undef})
  $ntp_restrict = lookup({'name'=>'ntp::restrict','default_value'=> undef})
  class { 'ntp':
    servers => $ntp_atomic,
    iburst_enable => true,
    restrict => $ntp_restrict
  }
}
