#site/profile/manifests/apt_sources_labunix.pp
class profile::apt_sources_labunix ($apt_repos) {
  include apt
  $apt_repos.each | String $apt_repo_name, Hash $attrs | 
  {
     apt::source { $apt_repo_name:
       * => $attrs,
    }
  }

}
