# Class profile::networker
class profile::networker {
  service { 'networker':
    ensure  => 'running',
    enable  => true,
    require => Package['lgtoclnt'],
  }

  package { 'lgtoclnt':
    ensure => 'latest'
  }
}
