class profile::samba {

  class {'samba::server':
      workgroup     => 'UQAM',
      server_string => "UQAM netconf",
      interfaces    => "ens192 lo",
      security      => 'user'
    }

  samba::server::share {'data':
    comment              => 'Data',
    path                 => '/srv/netconf',
    browsable            => true,
    writable             => true,
    create_mask          => 774,
    directory_mask       => 774,
    force_group          => 'users',
    force_user           => 'netconf'
    }
  }
