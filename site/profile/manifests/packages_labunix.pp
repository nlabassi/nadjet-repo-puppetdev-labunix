# Class with all the linux admins
class profile::packages_labunix ($packages) {
  $packages.each | String $package_name, Hash $attrs | 
  {
     package { $package_name:
       * => $attrs,
    }
  }

}
