class profile::ssh_authorized_keys_labunix ($authorized_keys) { 

  $authorized_keys.each | String $key, Hash $attrs | 
  {
    ssh_authorized_key { $key:
      * => $attrs,
    }
  }

}
