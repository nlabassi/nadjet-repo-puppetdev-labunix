# Class with all the linux admins
class profile::fail2ban {
  class { 'fail2ban':
    require => Package['redhat-lsb']
  }
}
