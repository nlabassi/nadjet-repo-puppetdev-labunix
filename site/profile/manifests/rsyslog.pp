# Class with all the linux admins
class profile::rsyslog {
  class { 'rsyslog::client': }

  # Désactivation de la résolution DNS

  $rsyslog_sysconf = $::osfamily ? {
    'Debian' => '/etc/default/rsyslog',
    'RedHat' => '/etc/sysconfig/rsyslog',
    default  => '/etc/sysconfig/rsyslog',
  }

  exec { 'rsyslog_disable_DNS':
    command => "sed -i \'s/=\"/=\"-x -Q /\' ${rsyslog_sysconf}",
    unless  => "grep \'SYSLOGD_OPTIONS.*-x -Q\' ${rsyslog_sysconf}",
    path    => '/bin',
    notify  => Service['rsyslog'],
    require => Package['rsyslog'],
  }

}
