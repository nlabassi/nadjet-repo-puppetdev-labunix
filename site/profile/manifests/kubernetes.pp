# Kubernetes profile
class profile::kubernetes {
  class { 'kubernetes': }
}

# Kubernetes master profile
class profile::kubernetes::master {
  class { 'kubernetes::master': }
}

# Kubernetes worker profile
class profile::kubernetes::worker {
  class { 'kubernetes::worker': }
}

# Kubernetes autoload profile
class profile::kubernetes::autoload {
  class { 'kubernetes::autoload': }
}

# Kubernetes kubeconfigs profile
class profile::kubernetes::kubeconfigs {
  class { 'kubernetes::kubeconfigs': }
}

# Kubernetes namespaces profile
class profile::kubernetes::namespaces {
  class { 'kubernetes::namespaces': }
}

# Kubernetes rolebindings profile
class profile::kubernetes::rolebindings {
  class { 'kubernetes::rolebindings': }
}

# Kubernetes volumes profile
class profile::kubernetes::volumes {

  $nfsvolumes = lookup({ 'name' => 'kubernetes::nfsvolumes', 'default_value' => undef})
  create_resources('profile::kubernetes::volume::create', $nfsvolumes)

  class { 'kubernetes::volumes': }
}

# Ressource pour créer les dossiers sur le montage NFS
define profile::kubernetes::volume::create (
  String $ensure               = 'present',
  String $namespace            = undef,
  String $volume               = undef,
  String $mountpath            = '/mnt/kubernetes',
  String $nfsserver            = 'cirrus.uqam.ca',
  Optional[Integer] $uid       = '1000',
  Optional[Integer] $gid       = '1000',
  Optional[String] $mode       = '644',
  ) {

  if $ensure == 'present' {
    $status = 'directory'
  } elsif $ensure == 'absent' {
    $status = 'absent'
  }

  if ! defined (File["${mountpath}/${namespace}"])  {
    file { "${mountpath}/${namespace}":
      ensure  => 'directory',
      require => Class['autofs'],
    }
  }
  file { "${mountpath}/${namespace}/${volume}":
    ensure  => $status,
    owner   => $uid,
    group   => $gid,
    mode    => $mode,
    force   => true,
    require => File["${mountpath}/${namespace}"]
  }
}

# Etcd profile
class profile::etcd {
  class { 'kubernetes::etcd': }
}

# Etcd join profile
class profile::etcd::join {
  class { 'kubernetes::etcd::join': }
}

# Etcd bootstrap profile
class profile::etcd::bootstrap {
  class { 'kubernetes::etcd::bootstrap': }
  class { 'kubernetes::etcd::backup': }
}
