# Class with all the linux admins
class profile::users_labunix ($users) { 

  $users.each | String $username, Hash $attrs | 
  {
    user { $username:
      * => $attrs,
    }
  }

}
