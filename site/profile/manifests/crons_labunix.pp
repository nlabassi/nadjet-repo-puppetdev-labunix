#site/profile/manifests/crons_labunix.pp
class profile::crons_labunix ($crons) {
  $crons.each | String $cron_name, Hash $attrs | 
  {
     cron { $cron_name:
       * => $attrs,
    }
  }

}
