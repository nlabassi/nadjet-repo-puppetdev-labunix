# Class to manage yum config and repos
class profile::yum_versionlock {
  class { 'yum::plugin::versionlock': }

  # notify{ "Value of hiera_packages: ${hiera_packages}": }
  if lookup({'name' => 'yum::versionlock::list', 'default_value' => undef}) {
    create_resources('yum::versionlock', lookup({'name' => 'yum::versionlock::list', 'default_values_hash' => undef}))
  }
}
