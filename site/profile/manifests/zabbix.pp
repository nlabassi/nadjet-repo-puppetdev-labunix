# Class with all the linux admins
class profile::zabbix {
  class { 'zabbix::agent': }

  class { 'zabbix::userparameter': }

  # Création du fichier xml pour importation semi-automatique
  file { "/data/partage/zabbix/${::hostname}.xml":
    ensure  => 'file',
    content => template('profile/zabbix/zabbix_import_template.xml.erb'),
    mode    => '0555',
    replace => 'no',
    require => Autofs::Mapfile['/etc/auto.direct'],
  }
}

# Class to manage yum config and repos
class profile::zabbix::yum_update {
  package { 'yum-plugin-post-transaction-actions':
    ensure => installed,
  }
  class { 'cron':
    manage_package => false,
  }
  file { '/etc/zabbix/zabbix_agentd.d/userparameter_yum.conf':
      ensure  => 'present',
      source  => 'puppet:///modules/profile/zabbix/yum_update/userparameter_yum.conf',
      owner   => 'root',
      group   => 'root',
      mode    => '0644',
      notify  => Service['zabbix-agent'],
      require => Package['zabbix-agent'],
    }

    file { '/usr/local/bin/kernel-waiting':
      ensure  => 'present',
      source  => 'puppet:///modules/profile/zabbix/yum_update/kernel-waiting',
      owner   => 'root',
      group   => 'root',
      mode    => '0755',
      notify  => Service['zabbix-agent'],
      require => Package['zabbix-agent'],
    }

    file { '/etc/yum/post-actions/update.action':
      ensure  => 'present',
      source  => 'puppet:///modules/profile/zabbix/yum_update/updates.action',
      owner   => 'root',
      group   => 'root',
      mode    => '0644',
      require => Package['yum-plugin-post-transaction-actions'],
    }
}

class profile::zabbix::puppet_agent {
  file { "/etc/zabbix/zabbix_agentd.d/userparameter_puppet.conf":
    ensure  => 'present',
    source  => 'puppet:///modules/profile/zabbix/puppet_agent/userparameter_puppet.conf',
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    notify  => Service['zabbix-agent'],
    require => Package['zabbix-agent'],
  }

  file { '/usr/local/bin/zabbix_puppet.rb':
    ensure => 'present',
    source => 'puppet:///modules/profile/zabbix/puppet_agent/zabbix_puppet.rb',
    owner  => 'root',
    group  => 'root',
    mode   => '0700',
  }
}
