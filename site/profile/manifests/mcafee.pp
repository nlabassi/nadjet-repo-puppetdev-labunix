# Class to manage yum config and repos
class profile::mcafee {
  #include paquets::unzip
  package { 'unzip': ensure => 'installed' }
  # Installation de l'agent McAfee
  exec { 'install_agent_mcafee':
    command => '/data/fichiers/securite/mcafee/install.sh -i',
    require => [ Autofs::Mapfile['/etc/auto.direct'], Package['unzip'] ],
    unless  => 'test -d /opt/McAfee', # On n'installe pas si le répertoire /opt/McAfee existe
    path    => [ '/bin', '/usr/bin' ],
  }

  exec { 'check_cma_service':
    command => '/etc/init.d/cma start',
    unless  => '/etc/init.d/cma status',
    require => Exec['install_agent_mcafee'],
  }

}
