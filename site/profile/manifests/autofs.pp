# Class with all the linux admins
class profile::autofs {
  class { 'autofs':
    require => Package['nfs-utils'],
  }

  package { 'nfs-utils':
    ensure => 'present',
  }
}
