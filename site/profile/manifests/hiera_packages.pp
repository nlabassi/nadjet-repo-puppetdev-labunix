# Define hiera resource types
class profile::hiera_packages {
  $hiera_packages = lookup({'name' => 'hiera_packages', 'default_value'=>undef})

  $defaults = {
    'ensure' => 'present',
  }

  # notify{ "Value of hiera_packages: ${hiera_packages}": }
  if $hiera_packages {
    create_resources('hiera::package', $hiera_packages, $defaults)
  }
}

# hiera::package type to allow installation of multiple package from hieradata
define hiera::package ( $ensure ) {
  package { $title:
    ensure => $ensure
  }
}
