# Class with all the linux admins
class profile::users {
  $users = lookup({'name' => 'users', 'default_value' => undef})

  if $users {

    $defaults = {
      'ensure' => 'present',
    }
    create_resources(accounts::user,$users, $defaults)

  } else {
    notify { 'No users to create': }
  }
}
