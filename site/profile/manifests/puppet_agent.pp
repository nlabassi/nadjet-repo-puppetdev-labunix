# Class to manage puppet_agent config and repos
class profile::puppet_agent {
  class { 'puppet_agent': }

  service { 'puppet':
    ensure => 'running',
    enable => true
  }

  file { '/etc/puppetlabs/puppet/puppet.conf':
    ensure  => 'present',
    content => template('profile/puppet.conf.erb'),
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    notify  => Service['puppet'],
  }
}
