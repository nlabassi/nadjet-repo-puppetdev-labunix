#site/profile/manifests/apt_ppas_labunix.pp
class profile::apt_ppas_labunix ($apt_ppas) {
  $apt_ppas.each | String $apt_ppa_name | 
  {
     apt::ppa { $apt_ppa_name: }
  }

}
