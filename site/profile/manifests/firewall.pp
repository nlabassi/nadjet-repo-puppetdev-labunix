# Profile to setup firewall
class profile::firewall {

  class { 'firewall': }

  $pre_rules = lookup({'name' => 'firewall::pre_rules', 'default_value'=>undef})
  $default_rules = lookup({'name' => 'firewall::default_rules', 'default_value'=>undef})

  # notify{ "Value of hiera_packages: ${hiera_packages}": }
  if $pre_rules {
    create_resources('firewall::rule', $pre_rules)
  }
  # notify{ "Value of hiera_packages: ${hiera_packages}": }
  if $default_rules {
    create_resources('firewall::rule', $default_rules)
  }

  firewallchain{ 'INPUT:filter:IPv4':
    ensure  => present,
    policy  => drop,
    require => Firewall::Rule['010 Allow established connections'],
  }
}

# Resource used to create rules
define firewall::rule (
  $chain = undef,
  $jump = undef,
  $table = undef,
  $mss = undef,
  $set_mss = undef,
  $gateway = undef,
  $proto = undef,
  $iniface = undef,
  $outiface = undef,
  $state = undef,
  $action = undef,
  $sport = undef,
  $dport = undef,
  $source = undef,
  $destination = undef,
  $tcp_flags = undef,
  $provider = undef,
  ){
  firewall { $title:
    chain       => $chain,
    jump        => $jump,
    table       => $table,
    mss         => $mss,
    set_mss     => $set_mss,
    gateway     => $gateway,
    proto       => $proto,
    iniface     => $iniface,
    outiface    => $outiface,
    state       => $state,
    action      => $action,
    sport       => $sport,
    dport       => $dport,
    source      => $source,
    destination => $destination,
    tcp_flags   => $tcp_flags,
    provider    => $provider
  }
}
