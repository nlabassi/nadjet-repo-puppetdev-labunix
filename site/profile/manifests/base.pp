#the base profile should include component modules that will be on all nodes
class profile::base {
  class { 'profile::users': }
  if $::osfamily == 'RedHat' {
    class { 'profile::yum': }
  }
  class { 'profile::hiera_packages': }
  -> class { 'profile::firewall': }
  -> class { 'profile::fail2ban': }
  class { 'profile::ntp': }
  class { 'profile::hosts': }
  class { 'profile::autofs': }
  class { 'profile::zabbix': }
  class { 'profile::resolv_conf': }
  class { 'profile::rsyslog': }
  class { 'profile::puppet_agent': }
  class { 'profile::mcafee': }
  class { 'profile::yum_versionlock': }
  class { 'profile::autoupdate': }
  class { 'profile::sudo': }
  class { 'profile::selinux': }
  class { 'profile::zabbix::yum_update': }
  class { 'profile::zabbix::puppet_agent': }
}
