class profile::tftp {

  # add the tftp class
  class { 'tftp':

    # tftp server options
    directory       => '/var/lib/tftpboot',
    address         => $::ipaddress,
    options         => '--timeout 60',
    inetd           => false,
    username        => 'nobody',
  }
}
