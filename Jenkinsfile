pipeline {
  agent {
    node {
      label 'master'
    }
  }
  environment {
    KUBECONFIG = credentials('svc_infra_puppet_config')
  }

  stages {
    stage('Check Kubernetes environments') {
      steps {
        script {
          PUPPET_CONTAINER = sh (script: "kubectl --kubeconfig=$KUBECONFIG -n infra-puppet-prod-01 get pods -l name=puppetmaster  --no-headers| awk '{print \$1}'", returnStdout: true).trim()
        }
          print 'Lookup master pod name...'
          sh "echo ${PUPPET_CONTAINER}"
          print 'Checking namespace existence...'
          sh "kubectl --kubeconfig=${KUBECONFIG} get ns infra-puppet-prod-01"
      }
    }

    stage('R10K check environments') {
      steps{
        print 'Lookup master pod name again...'
        sh "echo ${PUPPET_CONTAINER}"
        print 'Syncing environments with r10k...'

        sh(script: "kubectl --kubeconfig=$KUBECONFIG -n infra-puppet-prod-01 exec -i ${PUPPET_CONTAINER} -- r10kuq / deploy environment", returnStdout:true)
      }
    }

    stage('R10k check & install puppetfile') {
      steps {
        print 'Checking puppetfile...'
        sh "kubectl --kubeconfig=$KUBECONFIG -n infra-puppet-prod-01 exec -i ${PUPPET_CONTAINER} -- r10kuq /etc/puppetlabs/code/environments/$BRANCH_NAME/ puppetfile check -v"
        sh "kubectl --kubeconfig=$KUBECONFIG -n infra-puppet-prod-01 exec -i ${PUPPET_CONTAINER} -- r10kuq /etc/puppetlabs/code/environments/$BRANCH_NAME/ puppetfile install --force -v"
      }
    }

    stage('R10K deploy environments') {
      steps {
        print 'Deploy environment with r10k...'
        sh "kubectl --kubeconfig=$KUBECONFIG -n infra-puppet-prod-01 exec -i ${PUPPET_CONTAINER} -- r10kuq /etc/puppetlabs/code/environments/$BRANCH_NAME/ deploy environment -v"
      }
    }

    stage('Puppet parse modules') {
      steps {
        print 'Checking puppet modules syntax...'
        sh "kubectl --kubeconfig=$KUBECONFIG -n infra-puppet-prod-01 exec -i ${PUPPET_CONTAINER} -- find /etc/puppetlabs/code/environments/$BRANCH_NAME/modules/ -name manifests -type d -exec puppet parser validate {} +"
      }
    }
  }

}
