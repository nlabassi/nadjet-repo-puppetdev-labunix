#!/bin/bash

RED='\033[0;31m'
GREEN='\033[0;32m'
NC='\033[0m' # No Color

verifier_reponse () {

  if [ $1 -eq 0 ]
  then
    printf "${GREEN}[v] $2 est bien installé${NC}\n"
  else
    printf "${RED}[x] $2 n'est pas été installé. Une erreure est survenue!${NC}\n"
  fi
}

installer_gitlint () {
  printf "${GREEN} - gitlint s'installe ... ${NC}\n"
  pip3 install -q gitlint
  sudo mv ~/.local/bin/gitlint /usr/bin
  verifier_reponse `echo $?` "gitlint"

  cd `git rev-parse --show-toplevel`
  printf "${GREEN} - gitlint comme commit-msg s'installe ... ${NC}\n"
  gitlint install-hook  >/dev/null
  verifier_reponse `echo $?` "gitlint comme git commit-msg hook"
}

DIR=`git rev-parse --show-toplevel`

for hook in `ls $DIR/misc/git-hooks --hide setup.sh`
do
  ln -s $DIR/misc/git-hooks/$hook $DIR/.git/hooks/
done

if ! command -v pip3 >/dev/null
then
   printf "${RED}Atention!!! \"pip\" n'est pas installé. \nVoulez vous l'installer maintentant? (y/n)${NC} "
   read reponse
   if [ $reponse = "y" ]; then
	   printf "${GREEN} - PIP s'installe ... ${NC}\n"
	   sudo apt install python3-pip
	   verifier_reponse `echo $?` "PIP"

     if ! command -v gitlint >/dev/null ; then
       installer_gitlint
	   fi
   else
       printf "${RED}Vous avez choisi n'est pas installe PIP${NC}\n"
   fi
else
  if ! command -v gitlint >/dev/null ; then
    installer_gitlint
  fi
fi
