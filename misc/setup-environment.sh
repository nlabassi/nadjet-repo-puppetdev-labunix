#!/bin/bash -e

RED="\033[31m"
YELLOW="\033[33m"
GREEN="\033[32m"
NC="\033[0m"


DIR="$(git rev-parse --show-toplevel)"

verifier_reponse () {
  if [ $1 -eq 0 ] ; then
    printf "${GREEN}[v] $2 est $3 ${NC}\n"
  else
    printf "${RED}[x] $2 n'a pas $4. Une erreure est survenue!${NC}\n"
  fi
}

install () {
  if ! command -v $1 >/dev/null ; then
    read -p "$1 n'est pas installé. Voulez vous l'installer maintentant? (y/n) " reponse
    if [ $reponse = "y" ] ; then
      printf "${GREEN} $1 s'installe ... ${NC}\n"
      sudo gem install $1
      verifier_reponse `echo $?` "\"$1\"" "bien installé" "été installé"
    fi
  fi
}

install_jq () {
  if ! command -v jq >/dev/null ; then
    wget -O jq https://github.com/stedolan/jq/releases/download/jq-1.6/jq-linux64
    sudo chmod +x ./jq
    sudo mv jq /usr/bin
  fi
}

# la liste des scripts a executer
printf "${GREEN}\n- Le script \"[misc/git-hook/setup.sh]\" est en train de s'executer ${NC}\n"
cd $DIR/misc/git-hooks/
./setup.sh
verifier_reponse `echo $?` "Le script \"[misc/git-hooks/setup.sh]\"" "bien executé" "été bien executé"

printf "${GREEN}- Le script \"[misc/script_alias_gitconfig.sh]\" est en train de s'executer ${NC}\n"
cd $DIR/misc/
./script_alias_gitconfig.sh
verifier_reponse `echo $?` "Le script \"[misc/script_alias_gitconfig.sh]\"" "bien executé" "pas été bien executé"

# la liste des dependeces

install puppet

install puppet-lint

install_jq
