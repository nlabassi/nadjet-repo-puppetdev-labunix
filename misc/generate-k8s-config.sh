#!/bin/bash

docker run --rm \
       -v $(pwd):/mnt \
       -e OS=centos \
       -e VERSION=1.10.1 \
       -e CONTAINER_RUNTIME=docker \
       -e CNI_PROVIDER=weave \
       -e CNI_VERSION='0.6.0' \
       -e FQDN=kube-244-89.si.uqam.ca \
       -e IP=132.208.244.89 \
       -e BOOTSTRAP_CONTROLLER_IP=kube-244-89 \
       -e ETCD_INITIAL_CLUSTER="etcd-kube-244-89=http://kube-244-89.si.uqam.ca:2380" \
       -e ETCD_IP="%{::ipaddress_ens192}" \
       -e KUBE_API_ADVERTISE_ADDRESS="%{::ipaddress_ens192}" \
       -e INSTALL_DASHBOARD=true  \
       puppet/kubetool
