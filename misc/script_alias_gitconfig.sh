#!/bin/bash -e

RED="\033[31m"
YELLOW="\033[33m"
GREEN="\033[32m"
END_COLOR="\033[0m"

if grep -Rq "alias" ~/.gitconfig
then

  if [ $(grep -c "ob =" ~/.gitconfig) -eq 0 ]
  then
    awk '1;/alias/{ print "   ob = \42!echo \047Les branches non-active > 2 semaines\047; for k in '\`'git branch -r|awk \047{print '\$'1}\047'\`';do if [ '\`'expr '\$'(date +%s) - '\$'(git show --pretty=format:\042%ct\042 '\$'k | head -n 1)'\`' -gt 1314871 ] && [ '\$'k != \047origin/production\047 ] && [ '\$'k != \047origin/HEAD\047 ]; then echo '\`'git show --pretty=format:\\\042%Cblue%cr %Cred%cn %Creset\\\042 '\$'k|head -n 1'\`''\$'k; fi done|sort -r\042"}' ~/.gitconfig > ~/.gitconfig.tmp && mv ~/.gitconfig.tmp ~/.gitconfig
    printf "${GREEN}L'alias \"ob\" a été ajouter avec succes!${END_COLOR}\n"
  else
    printf "${YELLOW}L'alias \"ob\" existe deja. Il a été mis à jour${END_COLOR}\n"
    OB=`grep -n "ob = " ~/.gitconfig | cut -f1 -d:`
    quot='\\\"'
    OB_ALIAS="  ob = \"!echo 'Les branches non-active > 2 semaines'; for k in \`git branch -r|awk '{print \$1}'\`;do if [ \`expr \$(date +%s) - \$(git show --pretty=format:\"%ct\" \$k | head -n 1)\` -gt 1314871 ] && [ \$k != 'origin/production' ] && [ \$k != 'origin/HEAD' ]; then echo \`git show --pretty=format:$quot%Cblue%cr %Cred%cn %Creset$quot \$k|head -n 1\`\$k; fi done|sort -r\""
    sed "$OB c\ $OB_ALIAS" ~/.gitconfig  > ~/.gitconfig.tmp && mv ~/.gitconfig.tmp ~/.gitconfig &> /dev/null
  fi

  if [ $(grep -c "ecart =" ~/.gitconfig) -eq 0 ]
  then
    awk '1;/alias/{ print "   ecart = \042!f() { echo \047 \\\\n La branche \047'\$'1\047(\047'\`'git rev-list --count '\$'1 --since=2.weeks'\`'\047 commits)\\\\n La branche \047'\$'2\047(\047'\`'git rev-list --count '\$'2 --since=2.weeks'\`'\047 commits)\\\\n Ecart est de \047'\$'(('\`'git rev-list --count '\$'1 --since=2.weeks'\`' - '\`'git rev-list --count '\$'2 --since=2.weeks'\`')) commits; }; f\042"}' ~/.gitconfig > ~/.gitconfig.tmp && mv ~/.gitconfig.tmp ~/.gitconfig
    printf "${GREEN}L'alias \"ecart\" a été ajouter avec succes!${END_COLOR}\n"
  else
    printf "${YELLOW}L'alias \"ecart\" existe deja. Il a été mis à jour${END_COLOR}\n"
    ECART=`grep -n "ecart =" ~/.gitconfig | cut -f1 -d:`
    v='\\\\n'
    ECART_ALIAS="  ecart = \"!f() { echo ' $v La branche '\$1'('\`git rev-list --count \$1 --since=2.weeks\`' commits)$v La branche '\$2'('\`git rev-list --count \$2 --since=2.weeks\`' commits)$v Ecart est de '\$((\`git rev-list --count \$1 --since=2.weeks\` - \`git rev-list --count \$2 --since=2.weeks\`)) commits; }; f\""
    sed -r "$ECART c\ $ECART_ALIAS" ~/.gitconfig  > ~/.gitconfig1.tmp && mv ~/.gitconfig1.tmp ~/.gitconfig &> /dev/null
  fi

  if [ $(grep -c "gr =" ~/.gitconfig) -eq 0 ]
  then
    awk '1;/alias/{print "   gr = log --graph --full-history --all --color --pretty=tformat:\"%x1b[31m%h%x09%x1b[32m%d%x1b[0m%x20%s%x20%x1b[33m(%an)%x1b[0m\""}' ~/.gitconfig > ~/.gitconfig.tmp && mv ~/.gitconfig.tmp ~/.gitconfig
    printf "${GREEN}L'alias \"gr\" a été ajouter avec succes!${END_COLOR}\n"
  else
    printf "${YELLOW}L'alias \"gr\" existe deja. Il a été mis à jour${END_COLOR}\n"
    GR=`grep -n "gr =" ~/.gitconfig | cut -f1 -d:`
    GR_ALIAS="  gr = log --graph --full-history --all --color --pretty=tformat:\"%x1b[31m%h%x09%x1b[32m%d%x1b[0m%x20%s%x20%x1b[33m(%an)%x1b[0m\""
    sed "$GR c\ $GR_ALIAS" ~/.gitconfig  > ~/.gitconfig2.tmp && mv ~/.gitconfig2.tmp ~/.gitconfig &> /dev/null
  fi

else
  echo '[alias]' >> ~/.gitconfig
  echo "   ob = \"!echo 'Les branches non-active > 2 semaines'; for k in \`git branch -r|awk '{print \$1}'\`;do if [ \`expr \$(date +%s) - \$(git show --pretty=format:\"%ct\" \$k | head -n 1)\` -gt 1314871 ] && [ \$k != 'origin/production' ] && [ \$k != 'origin/HEAD' ]; then echo \`git show --pretty=format:\\\"%Cblue%cr %Cred%cn %Creset\\\" \$k|head -n 1\`\$k; fi done|sort -r\"" >> ~/.gitconfig
  echo "   ecart = \"!f() { echo ' \\\\n La branche '\$1'('\`git rev-list --count \$1 --since=2.weeks\`' commits)\\\\n La branche '\$2'('\`git rev-list --count \$2 --since=2.weeks\`' commits)\\\\n Ecart est de '\$((\`git rev-list --count \$1 --since=2.weeks\` - \`git rev-list --count \$2 --since=2.weeks\`)) commits; }; f\"" >> ~/.gitconfig
  echo "   gr = log --graph --full-history --all --color --pretty=tformat:\"%x1b[31m%h%x09%x1b[32m%d%x1b[0m%x20%s%x20%x1b[33m(%an)%x1b[0m\"" >> ~/.gitconfig
  printf "${YELLOW}Les alias ont ete ajouter avec succes!${END_COLOR}\n"
fi
